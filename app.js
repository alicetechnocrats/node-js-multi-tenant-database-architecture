const express = require('express');
const bodyParser = require('body-parser');
const circularJSON = require('circular-json');
const app = express();
const apiRoutes = require('./routes/userroutes');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(apiRoutes);

app.listen(3000, () => {
    console.log("Server started.");
});