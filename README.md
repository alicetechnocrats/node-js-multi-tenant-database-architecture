## Project Description
Multi Tenant architecture, i.e multiple database with single code base on run time request
Best For SAAS architecture applications


## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

## Install Dependency

``Npm install``


## Run a Code
``node app.js``



## How To Test 
Create 5 or atlest 2 differant DataBase in mongo db

go to database >> mongo.js replace "connectionString" to your original one

now run "api/dbs/:dbname" Api via postman or related tool

api url will be like "http://localhost:3000/api/dbs/db1" and in second call it will be "http://localhost:3000/api/dbs/db2" ect

## Feel Free to improvements
alicetechnocrats@gmail.com
Website : **www.alicetechnocrats.com**
