exports.storeData = (req, res, next) => {
    const dbName = req.params.dbname;
    
    console.log(dbName);
    let conn = require('../database/mongo').db;

    var mongoose = require('mongoose');
    var db2 = conn.useDb(`${dbName}`);

    var postSchema = new mongoose.Schema({
        title: String,   
        date: Date
    });

    const Posts = db2.model("Posts", postSchema);
    const addpost = new Posts({
        title: "Manually Added Post"
       });
       // Hooking post to DB
       addpost.save();

    //const posts = Posts.find();
    
    mongoose.connection.close();
    //    mongoose.cnn.close();//close the connection
    res.status(200);
    //res.json(posts);
    res.json({ data: 'ok' });
    // res.json(postsall);
    res.end();
};