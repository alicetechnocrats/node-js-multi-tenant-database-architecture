const express = require('express');
const routes = express.Router({mergeParams: true});
const dbController = require('../controller/dbController');

routes.get('/api/dbs/:dbname', dbController.storeData);

module.exports = routes;