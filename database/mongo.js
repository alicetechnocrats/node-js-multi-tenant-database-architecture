const mongoose = require('mongoose');
// Replace your mongo db first database connection string 
const connectionString = "mongodb://localhost:27017/db1";
// bydefault merchant DB will be connected. We are working on 2 DBS admin & merchant
var db = mongoose.createConnection(connectionString,{ useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false });

exports.db = db;
